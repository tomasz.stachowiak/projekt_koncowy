import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginSteps {
    WebDriver webDriver;
    @Before
    public void iOpenBrowser() {
        System.setProperty("webdriver.chrome", "C:\\Webdrivers\\chromedriver.exe");
        webDriver = new ChromeDriver();
    }
    @After
    public void closeDriver() {
        webDriver.quit();
    }
    @Given("I go to login page")
    public void iGoToLoginPage() {
        webDriver.get("https://www.automationexercise.com/login");
    }
    @When("I type {string} as correct login")
    public void iTypeAsLogin(String login) {
        webDriver.findElement(By.name("email")).sendKeys(login);
    }
    @And("I type {string} as correct password")
    public void iTypeAsPassword(String password) {
        webDriver.findElement(By.name("password")).sendKeys(password);
    }
    @When("I type {string} as login")
    public void iTypeEmailAsIncorrectLogin(String email) {
        webDriver.findElement(By.name("email")).sendKeys(email);
    }

    @And("I type {string} as password")
    public void iTypePasswordAsIncorrectPassword(String password) {
        webDriver.findElement(By.name("password")).sendKeys(password);
    }

    @And("I click login button")
    public void iClickLoginButton() {
        webDriver.findElement(By.xpath("/html/body/section/div/div/div[1]/div/form/button")).click();
    }

    @Then("I am logged in")
    public void iAmLoggedIn() {
        Assert.assertTrue(webDriver.getPageSource().contains(" Logged in as "));
        webDriver.findElement(By.xpath("/html/body/header/div/div/div/div[2]/div/ul/li[4]/a")).click();
    }

    @Then("I see {string} and I {string}")
    public void verifyTexts(String text, String text2) {
         Assert.assertTrue(webDriver.getPageSource().contains(text));
         if (text.equals(" Logged as ")) {
            webDriver.findElement(By.xpath("/html/body/header/div/div/div/div[2]/div/ul/li[4]/a")).click();
        }
    }
}
