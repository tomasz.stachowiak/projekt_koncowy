Feature: Login user

  Scenario: Correct Credentials
    Given I go to login page
    When I type "a@wp.pl" as correct login
    And I type "aa" as correct password
    And I click login button
    Then I am logged in

  Scenario Outline: Variable Credentials
    Given I go to login page
    When I type <email> as login
    And I type <password> as password
    And I click login button
    Then  I see <text> and I <text2>
    Examples:
      | email      | password   | text                                   | text2        |
      | "aa@wp.pl" |   "aa"     | "Your email or password is incorrect!" | "not logged" |
      | "a@wp.pl"  |   "bb"     | "Your email or password is incorrect!" | "not logged" |
      | "a@wp.pl"  |   "aa"     | " Logged in as "                       | "am logged"  |


