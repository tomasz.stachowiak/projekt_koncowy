from behave import *
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

driver = webdriver.Chrome()
vars = {}
driver.get("https://www.automationexercise.com/")
driver.maximize_window()


@given('Click Singup')
def step_1(context):
    driver.find_element(By.LINK_TEXT, "Signup / Login").click()


@when('User enter "{name}" and "{password}"')
def step_2(context, name, password):
    driver.find_element(By.NAME, "email").send_keys(name)
    driver.find_element(By.NAME, "email").send_keys(Keys.ENTER)
    driver.find_element(By.NAME, "password").send_keys(password)
    driver.find_element(By.NAME, "password").send_keys(Keys.ENTER)


@then('User see "{wynik}"')
def step_3(context, wynik):
    driver.find_element(By.LINK_TEXT, wynik)
    driver.find_element(By.LINK_TEXT, "Logout").click()
    pass
