Feature: As user I want to login

    Scenario Outline: Login
        Given Click Singup
        When User enter "<name>" and "<password>"
        Then User see "<wynik>"
        Examples:
            | name    | password | wynik          |
            | a@wp.pl | cc       | Logged in as   |		
            | a@wp.pl | aa       | Logged in as A |
            | a@wp.pl | bb       | Logged in as   |

